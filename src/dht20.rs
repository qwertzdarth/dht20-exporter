use rppal::i2c::I2c;
use std::error::Error;

const ADDR_DHT20: u16 = 0x38;

pub struct DHT20<'a> {
    i2c: &'a mut I2c,
}

pub struct ReadResult {
    pub temperature: f32,
    pub humidity: f32,
}

impl <'a> DHT20<'a> {
    pub fn new(i2c: &'a mut I2c) -> Result<DHT20<'a>, Box<dyn Error>> {
        i2c.set_slave_address(ADDR_DHT20)?;
        Ok(DHT20 { i2c })
    }

    pub fn get_status(&mut self) -> Result<u8, Box<dyn Error>> {
        self.i2c.write(&[0x71])?;

        let mut status_result: [u8; 1] = [0];
        self.i2c.read(&mut status_result)?;

        Ok(status_result[0])
    }

    fn is_measuring(&mut self) -> Result<bool, Box<dyn Error>> {
        let status = self.get_status()?;
        Ok(status & 0x80 == 0x80)
    }

    pub fn read(&mut self) -> Result<ReadResult, Box<dyn Error>> {
        self.i2c.write(&[0xAC, 0x33, 0x00])?;

        std::thread::sleep(std::time::Duration::from_millis(100));

        while self.is_measuring()? {
            std::thread::sleep(std::time::Duration::from_millis(5));
        }

        let mut result: [u8; 7] = [0; 7];
        self.i2c.read(&mut result)?;


        let mut st: u32 = 0;
        st += (result[3] & 0x0F) as u32;
        st = st << 8;

        st += result[4] as u32;
        st = st << 8;

        st += result[5] as u32;

        let temp: f32 = 200.0 * (st as f32) / (2.0f32.powi(20)) - 50.0;

        let mut srh: u32 = result[1] as u32;
        srh = srh << 8;

        srh += result[2] as u32;
        srh = srh << 4;

        srh += ((result[3] & 0x0F) >> 4) as u32;

        let rh = (srh as f32) / 2.0f32.powi(20);

        let res = ReadResult {
            temperature: temp,
            humidity: rh,
        };

        Ok(res)
    }
}
