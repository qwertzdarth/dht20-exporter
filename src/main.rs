use async_std::prelude::FutureExt;
use prometheus_client::encoding::text::{encode, SendSyncEncodeMetric};
use prometheus_client::metrics::family::Family;
use prometheus_client::metrics::gauge::Gauge;
use prometheus_client::registry::Registry;
use rppal::i2c::I2c;
use std::sync::Arc;
use async_std::task;

mod dht20;

use dht20::DHT20;

#[derive(Clone)]
struct State {
    registry: Arc<Registry<Box<dyn SendSyncEncodeMetric>>>,
}

#[async_std::main]
async fn main() -> tide::Result<()> {
    let mut registry = <Registry>::default();

    let temp_fam = Family::<(), Gauge<f64>>::default();
    registry.register("temperature", "current temperature", Box::new(temp_fam.clone()));

    let humidity_fam = Family::<(), Gauge<f64>>::default();
    registry.register("humidity", "current relative humidity", Box::new(humidity_fam.clone()));

    let task_handle = task::spawn_blocking(move || {
        let mut i2c = I2c::with_bus(1).expect("could not access i2c bus");
        let mut dht20 = DHT20::new(&mut i2c).expect("could not communicate with dht20 sensor");

        let status = dht20.get_status().expect("could not get dht20 status");
        eprintln!("status       : 0x{:0X}", status);
        eprintln!("status & 0x18: 0x{:0X}", status & 0x18);

        loop {
            match dht20.read() {
                Err(e) => {
                    eprintln!("could not read from dht20: {}", e);
                },
                Ok(result) => {
                    temp_fam.get_or_create(&()).set(result.temperature as f64);
                    humidity_fam.get_or_create(&()).set(result.humidity as f64);
                }
            }

            std::thread::sleep(std::time::Duration::from_millis(500));
        };
    });

    let mut app = tide::with_state(State { registry: Arc::new(registry) });
    app.at("/metrics").get(|req: tide::Request<State>| async move {
        let mut encoded = vec![];
        encode(&mut encoded, &req.state().registry).unwrap();

        let response = tide::Response::builder(200)
                .body(encoded)
                .content_type("application/openmetrics-text; version=1.0.0; charset=utf-8")
                .build();
        Ok(response)
    });

    let app_handle = app.listen("0.0.0.0:10123");

    app_handle.join(task_handle).await.0?;

    Ok(())
}
